/**
 * Padding HEX component if necessary
 * Hex component representation requires
 * two hexadeecimal characters
 * @param {string} comp 
 * @returns {string} two hexadecimal characters
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" + comp;
    return padded;
};

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., "#00ff00"
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * HEX-to-RGB converion
 * @param {string} hex Hexadecimal color code (e.g., "#00ff00")
 * @returns {object} Object containing RGB values {r, g, b}
 */
export const hex_to_rgb = (hex) => {
    // Remove #
    hex = hex.replace("#", "");
    // Expand shorthand hex values
    if (hex.length === 3) {
        hex = hex.split('').map(c => c + c).join('');
    }
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    return { r, g, b };
};

// const hexColor = "#00ff00";
// const rgb = hex_to_rgb(hexColor);
// console.log(rgb);

